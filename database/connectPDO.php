<?php
$serverName = "localhost";
$username = "pitUser";
$password = "pitUser";
$database = "pit";

try {
    $conn = new PDO("mysql:host=$serverName;dbname=$database", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);		//Forces MySQL to create the PDO prepared statements instead of letting PDO make them.  Better SQL Injection protection
    //echo "Connected successfully"; 	//TESTING ONLY!
    }
catch(PDOException $e)
    {
    	//echo "Connection failed: " . $e->getMessage();	//TESTING ONLY!

		error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
		error_log(var_dump(debug_backtrace()));
		
		//Clean up any variables or connections that have been left hanging by this error.		
		
		header('Location: ../files/505_error_response_page.php');	//sends control to a User friendly page			
    }
		
/* //Development testing code using mysqli process
	//$link = mysqli_connect($serverName, $username, $password, $database);
if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
else
{
	echo "<h1>Connected mysqli</h1>";	
}
*/
?>