<?php
session_start();
//if ($_SESSION['validUser'] == "yes")	//If this is a valid user allow access to this page
//{
		
	//Setup the variables used by the page
		//field data
		$contact_name = "";
		$contact_email = "";
		$contact_reason = "";
		$contact_comments = "";
		//error messages
		$nameErrMsg = "";
		$emailErrMsg = "";
		$reasonErrMsg = "";
		$commentsErrMsg = "";
		
		$validForm = false;
				
	if(isset($_POST["submit"]))
	{	
		//The form has been submitted and needs to be processed
		
		
		//Validate the form data here!
	
		//Get the name value pairs from the $_POST variable into PHP variables
		//This example uses PHP variables with the same name as the name atribute from the HTML form
		$contact_name = $_POST['contact_name'];
		$contact_email = $_POST['contact_email'];
		$contact_reason = $_POST['contact_reason'];
		$contact_comments = $_POST['contact_comments'];
		$contact_contact = $_POST['contact_contact'];

		/*	FORM VALIDATION PLAN
		
			FIELD NAME		VALIDATION TESTS & VALID RESPONSES
			Name			Required Field		May not be empty
			Email			Required Field		Format
			Purpose			Required Field		Must have something checked
			Comments		Required ONLY IF 'Other' reason has been selected. 
		*/
		
		//VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.  
			function validateName($inName)
			{
				global $validForm, $nameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
				$nameErrMsg = "";
				
				if($inName == "")
				{
					$validForm = false;
					$nameErrMsg = "Name cannot be spaces";
				}
			}//end validateName()

			function validateEmail($inEmail)
			{
				global $validForm, $emailErrMsg;			//Use the GLOBAL Version of these variables instead of making them local
				$emailErrMsg = "";							//Clear the error message. 
				
				// Remove all illegal characters from email
				$inEmail = filter_var($inEmail, FILTER_SANITIZE_EMAIL);

				// Validate e-mail
				$inEmail = filter_var($inEmail, FILTER_VALIDATE_EMAIL);

				if($inEmail === false)
				{
					$validForm = false;
					$emailErrMsg = "Invalid email"; 					
				}
			}//end validateEmail()			
			
			function validateReason($inReason)
			{
				global $validForm, $reasonErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
				$reasonErrMsg = "";
				
				if($inReason == "")
				{
					$validForm = false;
					$reasonErrMsg = "Please select a reason for your contact";
				}
			}//end validateName()			
			
			function validateComments($inReason, $inComments)
			{
				global $validForm, $commentsErrMsg;
				$commentsErrMsg = "";
				
				if($inReason == "Other")
				{
					//Must have comments
					if($inComments == "")
					{
						$validForm = false;
						$commentsErrMsg = "Please explain what you need from us";	
					}				
				}
			}//end validateComments()

		//VALIDATE FORM DATA  using functions defined above
		
		if($contact_contact)	//HoneyPot form verification.  If this field contains content a bot has most likely submitted the form
		{
			header('Location: index.php');	//sends control back to the home page					
		}
		
		$validForm = true;		//switch for keeping track of any form validation errors
		
		validateName($contact_name);
		validateEmail($contact_email);
		validateReason($contact_reason);
		validateComments($contact_reason, $contact_comments);
		
		if($validForm)
		{
			$message = "All good";	
			
			try {
				
				require 'database/connectPDO.php';	//CONNECT to the database
				
				//mysql DATE stores data in a YYYY-MM-DD format
				$todaysDate = date("Y-m-d");		//use today's date as the default input to the date( )
				
				//Create the SQL command string
				$sql = "INSERT INTO pit_contacts (";
				$sql .= "contact_name, ";
				$sql .= "contact_email, ";
				$sql .= "contact_reason, ";
				$sql .= "contact_comments, ";
				$sql .= "contact_timestamp "; //Last column does NOT have a comma after it.
				$sql .= ") VALUES (:contactName, :email, :reason, :comments, UTC_TIMESTAMP)";
				
				//PREPARE the SQL statement
				$stmt = $conn->prepare($sql);
				
				//BIND the values to the input parameters of the prepared statement
				$stmt->bindParam(':contactName', $contact_name);
				$stmt->bindParam(':email', $contact_email);		
				$stmt->bindParam(':reason', $contact_reason);	
				$stmt->bindParam(':comments', $contact_comments);
				//$stmt->bindParam(':contactTime', CURRENT_TIMESTAMP);	//NOT REQUIRED - Passing the value in the statement above as a MYSQL default date/time value
				
				//EXECUTE the prepared statement
				$stmt->execute();	
				
				$message = "Thank you for your contact. We will contact you as soon as possible.";
				
				//Could insert code here to email a confirmation to the customer OR send an email to Customer Support staff. 
			}
			
			catch(PDOException $e)
			{
				$message = "There has been a problem. The system administrator has been contacted. Please try again later.";
	
				error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
				error_log(var_dump(debug_backtrace()));	
				//Clean up any variables or connections that have been left hanging by this error.		
		
				header('Location: files/505_error_response_page.php');	//sends control to a User friendly page					
			}

		}
		else
		{
			$message = "Something went wrong";
		}//ends check for valid form		

	}
	else
	{
		//Form has not been seen by the user.  display the form
	}// ends if submit 
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Presenting Information Technology</title>

	<link rel="stylesheet" href="css/pit.css">

</head>

<body>

<div id="container">

	<header>
    	<h1>Presenting Information Technology</h1>
    </header>
    
	<?php require 'includes/navigation.php' ?>
    
    <main>
    
        <h1>Contact Us</h1>
		<?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
        ?>
            <h1><?php echo $message ?></h1>
        
        <?php
			}
			else	//display form
			{
        ?>
        <form id="contactForm" name="contactForm" method="post" action="contactForm.php">
        	<fieldset>
              <legend>Contact Us</legend>
              <p>
                <label for="contact_name">Name: </label>
                <input type="text" name="contact_name" id="contact_name" value="<?php echo $contact_name;  ?>"> 
                <span class="errMsg"> <?php echo $nameErrMsg; ?></span>
              </p>
              <p>
                <label for="contact_email">Email: </label>
                <input type="text" name="contact_email" id="contact_email" value="<?php echo $contact_email;  ?>">
                <span class="errMsg"><?php echo $emailErrMsg; ?></span>
              </p>
              <p>
                <label for="contact_reason">Purpose: </label>
                <select name="contact_reason" id="contact_reason">
                  <option value="" <?php if($contact_reason == "") { echo "selected";} ?> >Please select one</option>
                  <option value="More" <?php if($contact_reason == "More") { echo "selected";} ?> >More Information</option>
                  <option value="Info" <?php if($contact_reason == "Info") { echo "selected";} ?> >Presenter Information</option>
                  <option value="Schedule" <?php if($contact_reason == "Schedule") { echo "selected";} ?> >Schedule Conflict</option>
                  <option value="Other" <?php if($contact_reason == "Other") { echo "selected";} ?> >Other</option>
                </select>
                <span class="errMsg"><?php echo $reasonErrMsg; ?></span>                
              </p>
              	<!-- Honeypot Form Protection -->
              <p id="contactCheck">	
                <label for="contact_contact">
                  <input type="text" name="contact_contact" id="contact_contact">
                </label>
              </p>  
              <p>
                <label for="contact_comments">Comments: </label>
                  <textarea name="contact_comments" id="contact_comments" maxlength="700"><?php echo $contact_comments; ?></textarea>

                <span class="errMsg"><?php echo $commentsErrMsg; ?></span>                
              </p>
              <p>&nbsp;</p>            
              
            </fieldset>
         	<p>
            	<input type="submit" name="submit" id="submit" value="Contact" />
            	<input type="reset" name="button2" id="button2" value="Clear Form" onClick="clearForm()" />
        	</p>  
        </form>
        <?php
			}//end else
        ?>    	
        
	</main>
    
	<footer>
    	<p>Copyright &copy; <script> var d = new Date(); document.write (d.getFullYear());</script> All Rights Reserved</p>
    
    </footer>




</div>
</body>
</html>
